# Sapphire GLS Grub Theme

I obtained this theme from [here](https://www.gnome-look.org/p/1484567) and made modifications. The license of this theme is the same as where we got it from.
